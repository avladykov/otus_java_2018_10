package ru.otus;

public class TestOne {
    @Before
    public void before() {
        System.out.println("before");
    }

    @Test
    public void testOne() {
        System.out.println("one");
    }

    @Test
    public void testTwo() {
        System.out.println("two");
    }

    @After
    public void after() {
        System.out.println("after");
    }
}
