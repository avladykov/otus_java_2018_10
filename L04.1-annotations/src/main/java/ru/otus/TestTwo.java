package ru.otus;

public class TestTwo {
    @Before
    void before() {
        System.out.println("before");
    }

    @Test
    void testOne() {
        System.out.println("one");
    }

    @Test
    void testTwo() {
        System.out.println("two");
    }

    @After
    void after() {
        System.out.println("after");
    }
}
