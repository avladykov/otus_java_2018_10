package ru.otus;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

class TestRunner {
    static void run(Class<?> testClass)
            throws NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException,
            InstantiationException {

        List<Method> beforeMethods = new ArrayList<>();
        List<Method> afterMethods = new ArrayList<>();
        List<Method> testMethods = new ArrayList<>();
        init(testClass, beforeMethods, afterMethods, testMethods);

        run(testClass, beforeMethods, afterMethods, testMethods);
    }

    private static void init(
            Class<?> testClass,
            List<Method> beforeMethods,
            List<Method> afterMethods,
            List<Method> testMethods) {

        Method[] methods = testClass.getDeclaredMethods();

        for (Method method : methods) {
            if (method.getAnnotation(Before.class) != null) {
                beforeMethods.add(method);
            } else if (method.getAnnotation(Test.class) != null) {
                testMethods.add(method);
            } else if (method.getAnnotation(After.class) != null) {
                afterMethods.add(method);
            }
        }
    }

    private static void run(
            Class<?> testClass,
            List<Method> beforeMethods,
            List<Method> afterMethods,
            List<Method> testMethods)
            throws InstantiationException,
            IllegalAccessException,
            InvocationTargetException,
            NoSuchMethodException {

        for (Method tm : testMethods) {
            Object instance = testClass.getConstructor().newInstance();

            try {
                for (Method bm : beforeMethods) {
                    bm.invoke(instance);
                }

                tm.invoke(instance);

                for (Method am : afterMethods) {
                    am.invoke(instance);
                }
            }
            catch (Exception e) {
                System.out.println(String.format("Error occurred: %s", e));
            }
        }
    }
}
