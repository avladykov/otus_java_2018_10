package ru.otus;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args)
            throws NoSuchMethodException,
            IllegalAccessException,
            InstantiationException,
            InvocationTargetException {

        TestRunner.run(TestOne.class);
        TestRunner.run(TestTwo.class);
    }
}
