public class Program {
    private final static int FIRST = 1;
    private final static int SECOND = 2;
    private static volatile int turn = -1;

    public static void main(String[] args) throws InterruptedException {
        var t1 = new Thread(new Counter(FIRST, SECOND));
        var t2 = new Thread(new Counter(SECOND, FIRST));

        t1.setName("1");
        t2.setName("2");

        t1.start();
        t2.start();

        turn = FIRST;

        t1.join();
        t2.join();
    }

    public static class Counter implements Runnable {
        private final static int MIN = 1;
        private final static int MAX = 10;
        private final int SELF;
        private final int OTHER;

        public Counter(int self, int other) {
            SELF = self;
            OTHER = other;
        }

        @Override
        public void run() {
            while (true) {
                for (var i = MIN; i < MAX * 2 - 1; i++) {
                    while (turn != SELF) {
                        Thread.onSpinWait();
                    }

                    if (i < MAX) {
                        print(i);
                    } else {
                        print(MAX * 2 - i);
                    }

                    sleep(500);

                    turn = OTHER;
                }
            }
        }

        private static void print(int counter) {
            var name = Thread.currentThread().getName();
            System.out.println(name + ": " + counter);
        }

        private static void sleep(int millis) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
