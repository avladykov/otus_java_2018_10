package ru.otus;

import com.google.common.base.*;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String str = Joiner.on(",").join(Arrays.asList(1, 2, 3));
        System.out.println(str);
    }
}
