package ru.otus;

public interface ElementCreator<T> {
    T create();
}
