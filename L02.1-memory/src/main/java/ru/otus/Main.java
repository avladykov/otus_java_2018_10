package ru.otus;

import java.util.Collections;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Measurer.measureInt();
        Measurer.measureLong();
        Measurer.measureDouble();
        Measurer.measure(Object::new);
        Measurer.measure(() -> new String(""));
        Measurer.measure(() -> new String(new byte[0]));
        Measurer.measure(() -> new String(new char[0]));
        Measurer.measure(() -> new int[0]);
        Measurer.measure(() -> new int[1]);
        Measurer.measure(() -> new int[2]);
        Measurer.measure(() -> new int[3]);
        Measurer.measure(() -> new int[4]);
        Measurer.measure(() -> new int[5]);
        Measurer.measure(() -> new int[6]);
        Measurer.measure(() -> new int[7]);
        Measurer.measure(() -> new int[8]);
        Measurer.measure(() -> new int[9]);
        Measurer.measure(() -> new int[10]);
        Measurer.measure(() -> new int[20]);
        Measurer.measure(() -> new int[30]);
        Measurer.measure(() -> Collections.nCopies(0, 1));
        Measurer.measure(() -> Collections.nCopies(1, 1));
        Measurer.measure(() -> Collections.nCopies(2, 1));
        Measurer.measure(() -> Collections.nCopies(3, 1));
        Measurer.measure(() -> Collections.nCopies(4, 1));
        Measurer.measure(() -> Collections.nCopies(5, 1));
        Measurer.measure(() -> Collections.nCopies(6, 1));
        Measurer.measure(() -> Collections.nCopies(7, 1));
        Measurer.measure(() -> Collections.nCopies(8, 1));
        Measurer.measure(() -> Collections.nCopies(9, 1));
        Measurer.measure(() -> Collections.nCopies(10, 1));
        Measurer.measure(() -> Collections.nCopies(20, 1));
        Measurer.measure(() -> Collections.nCopies(30, 1));
    }
}
