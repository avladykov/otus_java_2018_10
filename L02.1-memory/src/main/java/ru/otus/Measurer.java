package ru.otus;

class Measurer {
    private static final int ARRAY_SIZE = 20_000_000;

    static void measureInt() throws InterruptedException {
        long memory = getAllocatedMemory();

        int[] array = new int[ARRAY_SIZE];

        System.out.println("Size of int: " + Math.round((getAllocatedMemory() - memory) / (double)array.length));
    }

    static void measureLong() throws InterruptedException {
        long memory = getAllocatedMemory();

        long[] array = new long[ARRAY_SIZE];

        System.out.println("Size of long: " + Math.round((getAllocatedMemory() - memory) / (double)array.length));
    }

    static void measureDouble() throws InterruptedException {
        long memory = getAllocatedMemory();

        double[] array = new double[ARRAY_SIZE];

        System.out.println("Size of double: " + Math.round((getAllocatedMemory() - memory) / (double)array.length));
    }

    static <T> void measure(ElementCreator<T> creator) throws InterruptedException {
        Object[] array = new Object[ARRAY_SIZE];

        long memory = getAllocatedMemory();

        for (int i = 0; i < ARRAY_SIZE; i++) {
            array[i] = creator.create();
        }

        System.out.println("Size of an element: " + Math.round((getAllocatedMemory() - memory) / (double)array.length));
    }

    private static long getAllocatedMemory() throws InterruptedException {
        System.gc();
        Thread.sleep(10);
        Runtime runtime = Runtime.getRuntime();
        return runtime.totalMemory() - runtime.freeMemory();
    }
}
