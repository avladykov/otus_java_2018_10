package ru.otus.atm;

import java.util.*;

public class SimpleATM implements ATM {
    private Map<Denomination, Integer> banknotes = new TreeMap<>(Comparator.reverseOrder());

    @Override
    public void deposit(Denomination denomination, int count) {
        if (count < 0) {
            throw new ATMException("Incorrect count");
        }

        var rest = banknotes.getOrDefault(denomination, 0);
        banknotes.put(denomination, rest + count);
    }

    @Override
    public Map<Denomination, Integer> withdraw(int amount) {
        if (amount <= 0) {
            throw new ATMException("Incorrect amount.");
        }

        var total = amount;
        var result = new HashMap<Denomination, Integer>();

        var snapshot = new TreeMap<Denomination, Integer>(Comparator.reverseOrder());
        snapshot.putAll(banknotes);

        for (var d : snapshot.keySet()) {
            var m = total / d.getValue();
            if (m > 0) {
                var count = Math.min(snapshot.get(d), m);
                total -= d.getValue() * count;
                snapshot.put(d, snapshot.get(d) - count);
                result.put(d, m);
            }
        }

        if (total != 0) {
            throw new ATMException("Not enough banknotes.");
        }

        banknotes = snapshot;

        return result;
    }

    @Override
    public Long total() {
        var total = 0L;
        for (var entry : banknotes.entrySet()) {
            total += entry.getKey().getValue() * entry.getValue();
        }

        return total;
    }
}
