package ru.otus.atm;

public enum  Denomination {
    DENOMINATION_50 (50),
    DENOMINATION_100 (100),
    DENOMINATION_200 (200),
    DENOMINATION_500 (500),
    DENOMINATION_1000 (1000),
    DENOMINATION_5000 (5000);

    private final int value;

    Denomination(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
