package ru.otus.atm;

public class ATMException extends RuntimeException {
    public ATMException(String errorMessage) {
        super(errorMessage);
    }
}
