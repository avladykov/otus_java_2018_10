package ru.otus.atm;

import java.util.Map;

public interface ATM {
    void deposit(Denomination denomination, int count);

    Map<Denomination, Integer> withdraw(int amount);

    Long total();
}
