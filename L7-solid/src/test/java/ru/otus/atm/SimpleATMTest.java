package ru.otus.atm;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SimpleATMTest {
    private final ATM atm = new SimpleATM();

    @Test
    void deposit() {
        // arrange
        // act
        atm.deposit(Denomination.DENOMINATION_1000, 1);

        // assert
        assertEquals(1000, atm.total());
    }

    @Test
    void deposit_incorrect_count() {
        try {
            // arrange
            // act
            atm.deposit(Denomination.DENOMINATION_1000, -1);

            fail("Expected an ATMException to be thrown.");
        } catch (ATMException e) {
            // assert
            assertEquals("Incorrect count", e.getMessage());
        }
    }

    @Test
    void withdraw() {
        // arrange
        atm.deposit(Denomination.DENOMINATION_50, 20);
        atm.deposit(Denomination.DENOMINATION_100, 1);
        atm.deposit(Denomination.DENOMINATION_500, 1);
        atm.deposit(Denomination.DENOMINATION_1000, 10);
        atm.deposit(Denomination.DENOMINATION_5000, 10);

        // act
        var banknotes = atm.withdraw(11750);

        // assert
        var expected = new HashMap<Denomination, Integer>();
        expected.put(Denomination.DENOMINATION_50, 3);
        expected.put(Denomination.DENOMINATION_100, 2);
        expected.put(Denomination.DENOMINATION_500, 1);
        expected.put(Denomination.DENOMINATION_1000, 1);
        expected.put(Denomination.DENOMINATION_5000, 2);
        assertThat(banknotes, is(expected));
        assertThat(atm.total(), is(49850L));
    }

    @Test
    void withdraw_incorrect_amount() {
        try {
            // arrange
            // act
            atm.withdraw(-1);

            fail("Expected an ATMException to be thrown.");
        } catch (ATMException e) {
            // assert
            assertEquals("Incorrect amount.", e.getMessage());
        }
    }

    @Test
    void withdraw_inappropriate_amount() {
        // arrange
        atm.deposit(Denomination.DENOMINATION_50, 1);
        atm.deposit(Denomination.DENOMINATION_100, 1);
        atm.deposit(Denomination.DENOMINATION_200, 1);

        try {
            // act
            atm.withdraw(315);

            fail("Expected an ATMException to be thrown.");
        } catch (ATMException e) {
            // assert
            assertEquals("Not enough banknotes.", e.getMessage());
            assertEquals(350, atm.total());
        }
    }

    @Test
    void total() {
        // arrange
        atm.deposit(Denomination.DENOMINATION_50, 5);
        atm.deposit(Denomination.DENOMINATION_100, 5);
        atm.deposit(Denomination.DENOMINATION_200, 5);
        atm.deposit(Denomination.DENOMINATION_500, 5);
        atm.deposit(Denomination.DENOMINATION_1000, 5);
        atm.deposit(Denomination.DENOMINATION_5000, 5);

        // act
        var total = atm.total();

        // assert
        assertEquals(34250, total);
    }
}