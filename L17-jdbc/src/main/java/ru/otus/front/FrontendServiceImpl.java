package ru.otus.front;

import ru.otus.app.FrontendService;
import ru.otus.app.MessageSystemContext;
import ru.otus.app.entities.User;
import ru.otus.app.messages.MsgCreateUser;
import ru.otus.messageSystem.Address;
import ru.otus.messageSystem.Message;
import ru.otus.messageSystem.MessageSystem;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

/**
 * Created by tully.
 */
public class FrontendServiceImpl implements FrontendService {
    private final Address address;
    private final MessageSystemContext context;
    private final List<User> users = new CopyOnWriteArrayList<>();
    private Consumer<List<User>> callback;

    public FrontendServiceImpl(MessageSystemContext context, Address address) {
        this.context = context;
        this.address = address;
    }

    public void init() {
        context.getMessageSystem().addAddressee(this);
    }

    @Override
    public Address getAddress() {
        return address;
    }

    public void createUser(User user) {
        Message message = new MsgCreateUser(getAddress(), context.getDbAddress(), user);
        context.getMessageSystem().sendMessage(message);
    }

    @Override
    public void addUser(User user) {
        users.add(user);
        notifyUsersUpdated(users);
    }

    @Override
    public void registerUserCreatedCallback(Consumer<List<User>> callback) {
        this.callback = callback;
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public MessageSystem getMS() {
        return context.getMessageSystem();
    }

    private void notifyUsersUpdated(List<User> users) {
        if (callback != null) {
            callback.accept(users);
        }
    }
}
