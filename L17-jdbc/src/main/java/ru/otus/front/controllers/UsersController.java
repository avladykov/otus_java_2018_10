package ru.otus.front.controllers;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.otus.app.FrontendService;
import ru.otus.app.entities.User;
import ru.otus.front.websocket.MsgUser;

import java.util.List;

@Controller
public class UsersController {

    private final FrontendService frontendService;
    private final SimpMessagingTemplate template;

    public UsersController(FrontendService frontendService, SimpMessagingTemplate template) {
        this.frontendService = frontendService;
        this.frontendService.registerUserCreatedCallback(this::userCreated);

        this.template = template;
    }

    @GetMapping("/")
    public String index(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("users", frontendService.getAllUsers());
        return "index";
    }

    @MessageMapping("/users")
    @SendTo("/topic/users")
    public void createUser(MsgUser request) {
        var user = new User(request.getName(), request.getAge());

        frontendService.createUser(user);
    }

    private void userCreated(List<User> users) {
        System.out.println("User created");

        var response = new MsgUser[users.size()];
        for (var i = 0; i < users.size(); i++) {
            response[i] = new MsgUser();
            response[i].setId(users.get(i).getId());
            response[i].setName(users.get(i).getName());
            response[i].setAge(users.get(i).getAge());
        }

        template.convertAndSend("/topic/users", response);
    }
}
