package ru.otus.app;

import ru.otus.app.entities.User;
import ru.otus.messageSystem.Addressee;

import java.util.List;
import java.util.function.Consumer;

/**
 * Created by tully.
 */
public interface FrontendService extends Addressee {
    void init();

    void createUser(User user);

    void addUser(User user);

    void registerUserCreatedCallback(Consumer<List<User>> callback);

    List<User> getAllUsers();
}

