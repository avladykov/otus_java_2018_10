package ru.otus.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.otus.app.DBService;
import ru.otus.app.FrontendService;
import ru.otus.app.MessageSystemContext;
import ru.otus.db.DBServiceImpl;
import ru.otus.db.orm.HibernateDataSource;
import ru.otus.db.orm.DataSource;
import ru.otus.front.FrontendServiceImpl;
import ru.otus.messageSystem.Address;
import ru.otus.messageSystem.MessageSystem;

@Configuration
@ComponentScan({"ru.otus.app", "ru.otus.db", "ru.otus.front", "ru.otus.messageSystem"})
public class BeanConfig {
    @Bean
    public DataSource jdbc() {
        return new HibernateDataSource();
    }

    @Bean
    public MessageSystemContext context(MessageSystem messageSystem) {
        var context = new MessageSystemContext(messageSystem);
        context.setFrontAddress(new Address("Frontend"));
        context.setDbAddress(new Address("DB"));
        return context;
    }

    @Bean
    public FrontendService frontendService(MessageSystemContext context) {
        var service = new FrontendServiceImpl(context, context.getFrontAddress());
        service.init();
        return service;
    }

    @Bean
    public DBService dbService(MessageSystemContext context) {
        var service = new DBServiceImpl(context, context.getDbAddress(), jdbc());
        service.init();
        return service;
    }
}
