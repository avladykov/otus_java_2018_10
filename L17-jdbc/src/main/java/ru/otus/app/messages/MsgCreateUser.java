package ru.otus.app.messages;

import ru.otus.app.DBService;
import ru.otus.app.MsgToDB;
import ru.otus.app.entities.User;
import ru.otus.messageSystem.Address;

/**
 * Created by tully.
 */
public class MsgCreateUser extends MsgToDB {
    private final User user;

    public MsgCreateUser(Address from, Address to, User user) {
        super(from, to);
        this.user = user;
    }

    @Override
    public void exec(DBService dbService) {
        dbService.createUser(user);
        dbService.getMS().sendMessage(new MsgCreateUserAnswer(getTo(), getFrom(), user));
    }
}
