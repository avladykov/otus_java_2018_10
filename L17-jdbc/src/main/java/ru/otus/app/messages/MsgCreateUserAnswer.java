package ru.otus.app.messages;

import ru.otus.app.FrontendService;
import ru.otus.app.MsgToFrontend;
import ru.otus.app.entities.User;
import ru.otus.messageSystem.Address;

/**
 * Created by tully.
 */
public class MsgCreateUserAnswer extends MsgToFrontend {
    private final User user;

    public MsgCreateUserAnswer(Address from, Address to, User user) {
        super(from, to);
        this.user = user;
    }

    @Override
    public void exec(FrontendService frontendService) {
        System.out.println("User: " + user.getName() + " has id: " + user.getId());
        frontendService.addUser(user);
    }
}
