package ru.otus.app;

import ru.otus.app.entities.User;
import ru.otus.messageSystem.Addressee;

/**
 * Created by tully.
 */
public interface DBService extends Addressee {
    void init();

    void createUser(User user);
}
