package ru.otus.db;

import ru.otus.app.DBService;
import ru.otus.app.MessageSystemContext;
import ru.otus.app.entities.User;
import ru.otus.db.orm.DataSource;
import ru.otus.messageSystem.Address;
import ru.otus.messageSystem.MessageSystem;

import java.sql.SQLException;

/**
 * Created by tully.
 */
public class DBServiceImpl implements DBService {
    private final Address address;
    private final MessageSystemContext context;
    private final DataSource jdbc;

    public DBServiceImpl(MessageSystemContext context, Address address, DataSource jdbc) {
        this.context = context;
        this.address = address;
        this.jdbc = jdbc;
    }

    public void init() {
        context.getMessageSystem().addAddressee(this);
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public MessageSystem getMS() {
        return context.getMessageSystem();
    }

    @Override
    public void createUser(User user) {
        try {
            jdbc.create(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
