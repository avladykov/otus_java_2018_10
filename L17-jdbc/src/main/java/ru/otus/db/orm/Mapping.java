package ru.otus.db.orm;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

final class Mapping {
    private String tableName;
    private Field id;
    private Set<Field> columns;

    private Mapping(String tableName, Field id, Set<Field> columns) {
        this.tableName = tableName;
        this.id = id;
        this.columns = columns;
    }

    static Function<String, Mapping> of(Object obj) {
        return (o) -> new Mapping(
                Reflect.getSimpleClassName(obj),
                Reflect.getAnnotatedField(obj, Id.class),
                Reflect.getNonAnnotatedFields(obj, Id.class));
    }

    String getTableName() {
        return tableName;
    }

    Object id(Object o) throws IllegalAccessException {
        return id.get(o);
    }

    String getIdColumnName() {
        return id.getName();
    }

    Set<String> getColumns() {
        return columns.stream().map(Field::getName).collect(Collectors.toSet());
    }

    Set<Field> getAllColumns() {
        var allColumns = new HashSet<Field>();
        allColumns.add(id);
        allColumns.addAll(columns);
        return allColumns;
    }
}
