package ru.otus.db.orm;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import ru.otus.app.entities.User;


public class SessionFactoryBuilder {
    public static SessionFactory build() {
        var configuration = new Configuration();
        configuration.addAnnotatedClass(User.class);

        var registry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure()
                .build();

        return configuration.buildSessionFactory(registry);
    }
}
