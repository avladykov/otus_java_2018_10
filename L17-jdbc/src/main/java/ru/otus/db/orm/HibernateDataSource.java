package ru.otus.db.orm;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class HibernateDataSource implements DataSource {
    @PersistenceContext
    private EntityManager em;

    @Override
    public <T> List<T> load(Class<T> clazz) {
        var query = em.getCriteriaBuilder().createQuery(clazz);
        var select = query.select(query.from(clazz));
        return em.createQuery(select).getResultList();
    }

    @Override
    public <T> T load(long id, Class<T> type) {
        return em.find(type, id);
    }

    @Override
    public <T> void create(T o) {
        em.persist(o);
    }

    @Override
    public <T> void update(T o) {
        em.merge(o);
    }

    @Override
    public <T> void createOrUpdate(T o) {
        em.merge(o);
    }
}
