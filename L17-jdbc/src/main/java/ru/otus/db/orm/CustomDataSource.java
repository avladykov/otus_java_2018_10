package ru.otus.db.orm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CustomDataSource implements DataSource {
    private final String SELECT = "SELECT * FROM %s WHERE %s = ?;";
    private final String INSERT = "INSERT INTO %s (%s) VALUES (%s);";
    private final String UPDATE = "UPDATE %s SET %s WHERE %s = ?;";
    private final String MERGE = "MERGE INTO %s (%s, %s) KEY (%s) VALUES (?, %s);";

    private final Connection connection;
    private final Map<String, Mapping> mappings = new HashMap<>();
    private final Map<String, String> selects = new HashMap<>();
    private final Map<String, String> inserts = new HashMap<>();
    private final Map<String, String> updates = new HashMap<>();
    private final Map<String, String> merges = new HashMap<>();

    public CustomDataSource(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T> List<T> load(Class<T> clazz) throws Exception {
        return null;
    }

    @Override
    public <T> T load(long id, Class<T> clazz) throws Exception {
        var obj = Reflect.newInstance(clazz);

        var name = Reflect.getSimpleClassName(obj);
        var mapping = mappings.computeIfAbsent(name, Mapping.of(obj));

        try (var stmt = prepareSelect(connection, obj, id, mapping)) {
            var rs = stmt.executeQuery();
            rs.next();

            for (var field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                field.set(obj, rs.getObject(field.getName()));
            }
        }

        return obj;
    }

    @Override
    public <T> void create(T obj) throws SQLException {
        var savepoint = connection.setSavepoint();
        try (var stmt = prepareInsert(connection, obj)) {
            stmt.executeUpdate();
            try (var rs = stmt.getGeneratedKeys()) {
                rs.next();
                Reflect.setId(obj, rs.getLong(1));
            }
        } catch (Exception e) {
            connection.rollback(savepoint);
        }
    }

    @Override
    public <T> void update(T obj) throws SQLException {
        var savepoint = connection.setSavepoint();
        try (var stmt = prepareUpdate(connection, obj)) {
            stmt.execute();
        } catch (Exception e) {
            connection.rollback(savepoint);
        }
    }

    @Override
    public <T> void createOrUpdate(T obj) throws SQLException {
        var savepoint = connection.setSavepoint();
        try (var stmt = prepateMerge(connection, obj)) {
            stmt.execute();
        } catch (Exception e) {
            connection.rollback(savepoint);
        }
    }

    private PreparedStatement prepareSelect(Connection conn, Object obj, long id, Mapping mapping) throws Exception {
        var name = Reflect.getSimpleClassName(obj);
        var sql = selects.computeIfAbsent(
                name,
                o -> String.format(
                    SELECT,
                    mapping.getTableName(),
                    mapping.getIdColumnName()));

        var stmt = conn.prepareStatement(sql);
        stmt.setObject(1, id);

        return stmt;
    }

    private PreparedStatement prepareInsert(Connection conn, Object obj) throws SQLException, IllegalAccessException {
        var name = Reflect.getSimpleClassName(obj);
        var mapping = mappings.computeIfAbsent(name, Mapping.of(obj));
        var sql = inserts.computeIfAbsent(
                name,
                o -> String.format(
                    INSERT,
                    mapping.getTableName(),
                    String.join(", ", mapping.getColumns()),
                    String.join(", ", Collections.nCopies(mapping.getColumns().size(), "?"))));

        var stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        applyValues(obj, mapping, stmt, 1);

        return stmt;
    }

    private PreparedStatement prepareUpdate(Connection conn, Object obj) throws Exception {
        var name = Reflect.getSimpleClassName(obj);
        var mapping = mappings.computeIfAbsent(name, Mapping.of(obj));
        var sql = updates.computeIfAbsent(
                name,
                o -> String.format(
                        UPDATE,
                        mapping.getTableName(),
                        mapping.getColumns().stream().map(s -> String.format("%s = ?", s)).collect(Collectors.joining(", ")),
                        mapping.getIdColumnName()));

        var stmt = conn.prepareStatement(sql);
        var last = applyValues(obj, mapping, stmt, 1);
        stmt.setObject(last, Reflect.getValue(obj, mapping.getIdColumnName()));

        return stmt;
    }

    private PreparedStatement prepateMerge(Connection conn, Object obj) throws IllegalAccessException, SQLException {
        var name = Reflect.getSimpleClassName(obj);
        var mapping = mappings.computeIfAbsent(name, Mapping.of(obj));
        var sql = merges.computeIfAbsent(
                name,
                o -> String.format(
                        MERGE,
                        mapping.getTableName(),
                        mapping.getIdColumnName(),
                        String.join(", ", mapping.getColumns()),
                        mapping.getIdColumnName(),
                        String.join(", ", Collections.nCopies(mapping.getColumns().size(), "?"))
                ));

        var stmt = conn.prepareStatement(sql);

        var index = 1;
        stmt.setObject(index++, Reflect.getValue(obj, mapping.getIdColumnName()));
        applyValues(obj, mapping, stmt, index);

        return stmt;
    }

    private int applyValues(Object obj, Mapping mapping, PreparedStatement stmt, int start) throws SQLException {
        var values = Reflect.getValues(obj, mapping.getColumns());
        for (var col : mapping.getColumns()) {
            stmt.setObject(start++, values.get(col));
        }

        return start;
    }
}
