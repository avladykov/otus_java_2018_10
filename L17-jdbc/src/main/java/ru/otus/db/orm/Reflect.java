package ru.otus.db.orm;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Reflect {
    private static final HashMap<Class<?>, String> tables = new HashMap<>();
    private static final HashMap<Class<?>, Field> ids = new HashMap<>();
    private static final HashMap<Class<?>, Set<Field>> fields = new HashMap<>();

    private static final Map<Class<?>, Mapping> mappings = new HashMap<>();

    private Reflect() { }

    static Reflect the(Object o) {
        return new Reflect();
    }

    static String getClassName(Object o) {
        return o.getClass().getName();
    }

    static String getSimpleClassName(Object o) {
        return o.getClass().getSimpleName();
    }

    static Field getAnnotatedField(Object o, Class<? extends Annotation> annotation) {
        return Stream.of(o.getClass().getDeclaredFields())
                .filter(field -> field.getAnnotation(annotation) != null)
                .peek(field -> field.setAccessible(true))
                .findFirst()
                .orElseThrow();
    }

    static Set<Field> getNonAnnotatedFields(Object o, Class<? extends Annotation> annotation) {
        return Stream.of(o.getClass().getDeclaredFields())
                .filter(field -> field.getAnnotation(annotation) == null)
                .peek(field -> field.setAccessible(true))
                .collect(Collectors.toSet());
    }

    static Map<String, Object> getValues(Object o, Set<String> fields) {
        return Stream.of(o.getClass().getDeclaredFields())
                .filter(field -> fields.contains(field.getName()))
                .peek(field -> field.setAccessible(true))
                .collect(Collectors.toMap(Field::getName, field -> {
                    try {
                        return field.get(o);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }));
    }

    static Object getValue(Object o, String name) throws IllegalAccessException {
        return Stream.of(o.getClass().getDeclaredFields())
                .filter(field -> field.getName() == name)
                .peek(field -> field.setAccessible(true))
                .findFirst()
                .orElseThrow()
                .get(o);
    }

    static <T> void setId(T obj, long id) throws IllegalAccessException {
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.getAnnotation(Id.class) != null) {
                field.set(obj, id);
            }
        }
    }

    static <T> T newInstance(Class<T> type)
            throws NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException,
            InstantiationException {
        return type.getDeclaredConstructor().newInstance();
    }
}
