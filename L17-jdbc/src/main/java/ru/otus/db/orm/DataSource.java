package ru.otus.db.orm;

import java.sql.SQLException;
import java.util.List;

public interface DataSource {
    <T> List load(Class<T> clazz) throws Exception;
    <T> T load(long id, Class<T> clazz) throws Exception;
    <T> void create(T obj) throws SQLException;
    <T> void update(T obj) throws SQLException;
    <T> void createOrUpdate(T obj) throws SQLException;
}
