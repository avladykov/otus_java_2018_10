package ru.otus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.otus.app.entities.User;
import ru.otus.messageSystem.MessageSystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@SpringBootApplication
@Transactional
public class Program implements CommandLineRunner {
    private MessageSystem messageSystem;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    public Program(MessageSystem messageSystem) {
        this.messageSystem = messageSystem;
    }

    public static void main(String[] args) {
        SpringApplication.run(Program.class, args);
    }

    @Override
    public void run(String... args) {

        try {

            var u = em.find(User.class, 1L);
            var a = (List<User>) em.createQuery("FROM User ", User.class).getResultList();
            System.out.println(u);

        } catch (Exception e) {
            e.printStackTrace();
        }

        messageSystem.start();
    }
}
