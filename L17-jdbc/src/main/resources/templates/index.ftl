<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript" src="/webjars/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/webjars/sockjs-client/sockjs.min.js"></script>
    <script type="text/javascript" src="/webjars/stomp-websocket/stomp.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
</head>
<p>

<p>
<h1>Users</h1>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Age</th>
    </tr>
    <#list model["users"] as user>
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.age}</td>
        </tr>
    </#list>
</table>
</p>

<h2>New user</h2>
<form method="post" action="" name="user">
    <div>
        <label for="name">Name:</label>
        <br/>
        <input type="text" id="name" name="name" />
    </div>
    <div>
        <label for="age">Age:</label>
        <br/>
        <input type="text" id="age" name="age" />
    </div>
    <div>
        <button type="submit">Submit</button>
    </div>
</form>
</body>
</html>