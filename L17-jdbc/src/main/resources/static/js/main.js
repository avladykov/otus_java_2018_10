var stompClient = null;

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/users', function (message) {
            const users = JSON.parse(message.body);
            $('table').find('tr:gt(0)').remove();
            users.forEach(user => {
                $('table')
                    .append($('<tr>')
                        .append($('<td>').text(user.id))
                        .append($('<td>').text(user.name))
                        .append($('<td>').text(user.age)));
            });
        });
    });
}

function send() {
    const name = $("input#name");
    const age = $("input#age");

    const user = {
        name: name.val(),
        age: age.val()
    };

    stompClient.send("/app/users", {}, JSON.stringify(user));

    name.val('');
    age.val('')
}

$(function () {
    connect();
    $("form").on('submit', function (e) {
        e.preventDefault();
        send();
    });
});