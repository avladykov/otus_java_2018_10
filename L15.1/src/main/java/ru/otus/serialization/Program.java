package ru.otus.serialization;

import java.util.Arrays;

public class Program {
    public static void main(String[] args) throws IllegalAccessException {
        var obj = new Person(111, "aaa", Arrays.asList("foo", "bar"), new int[] {1, 2, 3}, new Person[]{
                new Person(222, "bbb", Arrays.asList("foo"), new int[] {1}, new Person[0]),
                new Person(333, "ccc", Arrays.asList("foo", "bar", "baz"), new int[] {1, 2}, new Person[]{
                        new Person(444, "ddd", Arrays.asList("foo"), new int[] {2, 3}, new Person[]{
                                new Person(555, "eee", Arrays.asList("foo", "bar"), new int[] {3}, new Person[0])
                        })
                })
        });

        var serializer = new JsonSerializer();
        var json = serializer.toJson(obj);
        System.out.println(json);
    }
}
