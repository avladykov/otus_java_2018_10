package ru.otus.serialization;

import java.util.List;

public class Person {
    private final static String CONST_STRING = "CONST_STRING";
    private final String nullable = null;
    private final int id;
    private final String name;
    private final List<String> interests;
    private final int[] scores;
    private final Person[] friends;
    Person(int id, String name, List<String> interests, int[] scores, Person[] friends) {
        this.name = name;
        this.id = id;
        this.interests = interests;
        this.scores = scores;
        this.friends = friends;
    }

    @Override
    public String toString() {
        return String.format("%s %s", id, name);
    }
}
