package ru.otus.serialization;

import java.io.StringWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

class JsonSerializer {
    private static final Set<Class<?>> PRIMITIVE_WRAPPERS = new HashSet<>();

    JsonSerializer() {
        initPrimitiveWrappers();
    }

    String toJson(Object obj) throws IllegalAccessException {
        var writer = new StringWriter();
        toJson(writer, obj);
        return writer.toString();
    }

    private void toJson(StringWriter w, Object obj) throws IllegalAccessException {
        if (obj == null) {
            writeNull(w);
            return;
        }

        var objClass = obj.getClass();
        if (objClass.isArray()) {
            writeJsonArray(w, obj);
        } else if (obj instanceof Collection) {
            writeCollection(w, obj);
        } else if (PRIMITIVE_WRAPPERS.contains(objClass)) {
            writeJsonValue(w, obj);
        } else if (objClass == String.class || objClass == Character.class) {
            writeJsonString(w, obj);
        } else {
            writeJsonObject(w, obj);
        }
    }

    private void writeNull(StringWriter w) {
        w.write("null");
    }

    private void writeJsonArray(StringWriter w, Object obj) throws IllegalAccessException {
        startJsonArray(w);

        var length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                w.write(',');
            }

            toJson(w, Array.get(obj, i));
        }

        endJsonArray(w);
    }

    private void writeCollection(StringWriter w, Object obj) throws IllegalAccessException {
        startJsonArray(w);

        var collection = (Collection)obj;
        boolean insertDelimiter = false;
        for (Object o : collection) {
            if (insertDelimiter) {
                w.write(',');
            }

            toJson(w, o);

            insertDelimiter = true;
        }

        endJsonArray(w);
    }

    private void writeJsonObject(StringWriter w, Object obj) throws IllegalAccessException {
        startJsonObject(w);

        boolean insertDelimiter = false;
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }

            Object fieldValue = field.get(obj);
            if (fieldValue == null) {
                continue;
            }

            if (insertDelimiter) {
                w.write(',');
            }

            w.write(String.format("\"%s\":", field.getName()));

            toJson(w, fieldValue);

            insertDelimiter = true;
        }

        endJsonObject(w);
    }

    private void writeJsonValue(StringWriter w, Object value) {
        w.write(value.toString());
    }

    private void writeJsonString(StringWriter w, Object value) {
        w.write(String.format("\"%s\"", value));
    }

    private void startJsonObject(StringWriter w) {
        w.write("{");
    }

    private void endJsonObject(StringWriter w) {
        w.write("}");
    }

    private void startJsonArray(StringWriter w) {
        w.write("[");
    }

    private void endJsonArray(StringWriter w) {
        w.write("]");
    }

    private void initPrimitiveWrappers() {
        PRIMITIVE_WRAPPERS.add(Byte.class);
        PRIMITIVE_WRAPPERS.add(Boolean.class);
        PRIMITIVE_WRAPPERS.add(Short.class);
        PRIMITIVE_WRAPPERS.add(Integer.class);
        PRIMITIVE_WRAPPERS.add(Long.class);
        PRIMITIVE_WRAPPERS.add(Float.class);
        PRIMITIVE_WRAPPERS.add(Double.class);
    }
}
