package ru.otus;

public class Main {
    public static void main(String[] args) {
        HwCache<Integer, Integer> cache = new MyCache<>(100);
        HwListener<Integer, Integer> listener =
                (key, value, action) -> System.out.println("key:" + key + ", value:" + value + ", action:" + action);
        cache.addListener(listener);
        cache.put(1,1);
        System.out.println(cache.get(1));
        cache.remove(1);
        cache.removeListener(listener);
    }
}
