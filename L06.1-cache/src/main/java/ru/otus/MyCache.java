package ru.otus;

import java.lang.ref.SoftReference;
import java.util.*;

public class MyCache<K, V> implements HwCache<K, V> {
    private final Map<K, SoftReference<V>> data;
    private final List<HwListener<K, V>> listeners = new ArrayList<>();
    private final int maxSize;

    MyCache(int maxSize) {
        this.maxSize = maxSize;
        data = new LinkedHashMap<>(maxSize);
    }

    @Override
    public void put(K key, V value) {
        if (data.size() == maxSize) {
            K firstKey = data.keySet().iterator().next();
            data.remove(firstKey);
        }

        data.put(key, new SoftReference<>(value));

        notify(key, value, "put");
    }

    @Override
    public void remove(K key) {
        SoftReference<V> value = data.remove(key);

        notify(key, value.get(), "remove");
    }

    @Override
    public V get(K key) {
        V value = data.get(key).get();

        notify(key, value, "get");

        return value;
    }

    @Override
    public void addListener(HwListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(HwListener listener) {
        listeners.remove(listener);
    }

    private void notify(K key, V value, String action) {
        listeners.forEach(listener -> {
            try {
                listener.notify(key, value, action);
            } catch (Exception e) {
                /* Do nothing */
            }
        });
    }
}