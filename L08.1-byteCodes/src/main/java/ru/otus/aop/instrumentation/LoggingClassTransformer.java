package ru.otus.aop.instrumentation;

import org.objectweb.asm.*;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;
import java.util.function.Consumer;
import java.util.function.Function;

public class LoggingClassTransformer implements ClassFileTransformer {
    @Override
    public byte[] transform(
            ClassLoader loader,
            String className,
            Class<?> classBeingRedefined,
            ProtectionDomain protectionDomain,
            byte[] classfileBuffer) {

        var cw = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);

        var cr = new ClassReader(classfileBuffer);
        cr.accept(new ClassScanner(cw), 0);

        return cw.toByteArray();
    }

    class ClassScanner extends ClassVisitor {

        ClassScanner(ClassWriter cw) {
            super(Opcodes.ASM7, cw);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            return new MethodScanner(super.visitMethod(access, name, descriptor, signature, exceptions), name);
        }
    }

    class MethodScanner extends MethodVisitor {

        private final String annotationDescriptor = "Lru/otus/aop/instrumentation/Log;";
        private final String methodName;

        MethodScanner(MethodVisitor mv, String methodName) {
            super(Opcodes.ASM7, mv);
            this.methodName = methodName;
        }

        @Override
        public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
            if (descriptor.equals(annotationDescriptor)) {
                this.visitCode();

                this.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
                this.visitLdcInsn(String.format("%s: ", methodName));
                this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "print", "(Ljava/lang/String;)V", false);

                this.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
                this.visitIntInsn(Opcodes.ALOAD, 1);
                this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "print", "(Ljava/lang/String;)V", false);

                this.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
                this.visitLdcInsn(", ");
                this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "print", "(Ljava/lang/String;)V", false);

                this.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
                this.visitIntInsn(Opcodes.ALOAD, 2);
                this.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);

                this.visitEnd();
            }

            return super.visitAnnotation(descriptor, visible);
        }
    }
}
