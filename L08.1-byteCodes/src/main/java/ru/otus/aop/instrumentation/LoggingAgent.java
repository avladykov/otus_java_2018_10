package ru.otus.aop.instrumentation;

import java.lang.instrument.Instrumentation;

public class LoggingAgent {
    public static void premain(String args, Instrumentation inst) {
        inst.addTransformer(new LoggingClassTransformer());
    }
}
