package ru.otus.aop.instrumentation;

class Logic {
    @Log
    String concat(String s1, String s2) {
        return s1 + s2;
    }
}
