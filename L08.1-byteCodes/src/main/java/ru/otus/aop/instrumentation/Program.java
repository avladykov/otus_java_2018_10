package ru.otus.aop.instrumentation;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Program {
    public static void main(String[] args) throws IOException, InvocationTargetException, IllegalAccessException {
        Logic logic = new Logic();
        System.out.println(logic.concat("one", "two"));
    }
}
