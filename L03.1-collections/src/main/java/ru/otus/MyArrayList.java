package ru.otus;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyArrayList<E> implements List<E> {
    private Object[] data;
    private int size;
    private int capacity;

    MyArrayList() {
        size = 0;
        capacity = 1;
        data = new Object[capacity];
    }

    public boolean add(E e) {
        ensureCapacity();

        data[size - 1] = e;

        return true;
    }

    public int size() {
        return size;
    }

    public E get(int index) {
        return (E) data[index];
    }

    public Object[] toArray() {
        Object[] result = new Object[size];
        System.arraycopy(data, 0, result, 0, size);
        return result;
    }

    public boolean addAll(Collection<? extends E> c) {
        for (E e : c) {
            add(e);
        }

        return true;
    }

    public ListIterator<E> listIterator() {
        return new ListIterator<E>() {
            private int current = -1;

            public boolean hasNext() {
                return current < size;
            }

            public E next() {
                return (E)data[++current];
            }

            public void set(E e) {
                data[current] = e;
            }

            public boolean hasPrevious() {
                throw new NotImplementedException();
            }

            public E previous() {
                throw new NotImplementedException();
            }

            public int nextIndex() {
                throw new NotImplementedException();
            }

            public int previousIndex() {
                throw new NotImplementedException();
            }

            public void remove() {
                throw new NotImplementedException();
            }

            public void add(E e) {
                throw new NotImplementedException();
            }
        };
    }

    public E remove(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }

        Object o = data[index];

        System.arraycopy(data, index + 1, data, index, size - index - 1);

        size -= 1;

        return (E)o;
    }

    public E set(int index, E element) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }

        data[index] = element;

        return element;
    }

    public boolean isEmpty() {
        throw new NotImplementedException();
    }

    public boolean contains(Object o) {
        throw new NotImplementedException();
    }

    public Iterator<E> iterator() {
        throw new NotImplementedException();
    }

    public <T1> T1[] toArray(T1[] a) {
        throw new NotImplementedException();
    }

    public boolean remove(Object o) {
        throw new NotImplementedException();
    }

    public boolean containsAll(Collection<?> c) {
        throw new NotImplementedException();
    }

    public boolean addAll(int index, Collection<? extends E> c) {
        throw new NotImplementedException();
    }

    public boolean removeAll(Collection<?> c) {
        throw new NotImplementedException();
    }

    public boolean retainAll(Collection<?> c) {
        throw new NotImplementedException();
    }

    public void clear() {
        throw new NotImplementedException();
    }

    public void add(int index, E element) {
        throw new NotImplementedException();
    }

    public int indexOf(Object o) {
        throw new NotImplementedException();
    }

    public int lastIndexOf(Object o) {
        throw new NotImplementedException();
    }

    public ListIterator<E> listIterator(int index) {
        throw new NotImplementedException();
    }

    public List<E> subList(int fromIndex, int toIndex) {
        throw new NotImplementedException();
    }

    private void ensureCapacity() {
        if (++size > capacity) {
            capacity *= 2;
            Object[] newData = new Object[capacity];
            if (size >= 0) {
                System.arraycopy(data, 0, newData, 0, size - 1);
            }

            data = newData;
        }
    }
}
