package ru.otus;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> s = new MyArrayList<>();

        // addAll()
        s.addAll(Arrays.asList("7", "8", "9"));
        s.addAll(Arrays.asList("0", "1", "2", "3", "4"));
        s.addAll(Arrays.asList("5", "6"));
        System.out.println(String.format("size: %d, data: %s", s.size(), Arrays.toString(s.toArray())));

        // remove()
        System.out.println(String.format("removed: %s, size: %d, data: %s", s.remove(0), s.size(), Arrays.toString(s.toArray())));
        System.out.println(String.format("removed: %s, size: %d, data: %s", s.remove(1), s.size(), Arrays.toString(s.toArray())));
        System.out.println(String.format("removed: %s, size: %d, data: %s", s.remove(s.size() - 1), s.size(), Arrays.toString(s.toArray())));

        // addAll()
        s.addAll(Arrays.asList("10", "20", "30", "40", "50"));

        // Collections.copy()
        List<String> d = new MyArrayList<>();
        for (int i = 0; i < s.size(); i++) { d.add(""); }
        Collections.copy(d, s);
        System.out.println(String.format("size: %d, data: %s", d.size(), Arrays.toString(d.toArray())));

        // Collections.sort()
        Collections.sort(s, Comparator.naturalOrder());
        System.out.println(String.format("size: %d, data: %s", s.size(), Arrays.toString(s.toArray())));
    }
}
